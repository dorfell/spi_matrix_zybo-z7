---------------------------------------------------------------------
----                                                             ----
---- file:    divisor_tb.vhd                                     ----
---- brief:   Module de Divisor Test bench                       ----
---- details: Module test bench pour divisor_tb.vhd.             ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2022/01/21                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
entity divisor_tb is
end divisor_tb;
 
 
architecture behavior of divisor_tb is 
 
  -- Component Declaration for the Unit Under Test (UUT)
  component divisor_1kHz is
    port ( clk,rst :  in std_logic;
           en_sck  :  in std_logic; 
           sck     : out std_logic);
  end component divisor_1kHz;

 
  --Inputs -----------------------------
  signal clk, rst, en_sck : std_logic;
  
  --Outputs -----------------------------
  -- sck SPI clock
  signal sck : std_logic := '0';

  
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
   constant clock_period : time := 8 ns;
 
 
begin
 
 
  -- Instantiate the Unit Under Test (UUT)
  uut: divisor_1kHz 
    port map(
      clk    => clk,
      rst    => rst,
      en_sck => en_sck,
      
      sck    => sck );


  -- Clock process definitions
  clock_process :process
    begin
	  clk <= '0';
      wait for clock_period/2;
      clk <= '1';
      wait for clock_period/2;
  end process;
 

  -- Stimulus process
  stim_proc: process
    begin		
      -- hold reset state for 8 ns x 2.
      wait for clock_period*2;

      -- insert stimulus here
      rst <= '0'; en_sck <= '0';
      wait for clock_period*2;
      
      
      rst <= '1'; en_sck <= '0';
      wait for clock_period*2;


      rst <= '0'; en_sck <= '0';
      wait for clock_period*5;

      rst <= '0'; en_sck <= '1';
      wait for 10 ms;

      rst <= '0'; en_sck <= '0';
      wait for 3 ms;
      

      wait;
   end process;

end;
