
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity mem_as1107 is
  port(
    mem_addr :  in std_logic_vector( 7 downto 0);
    mem_data : out std_logic_vector(15 downto 0) );
end mem_as1107;

architecture Behavioral of mem_as1107 is

signal sg_mem_data : std_logic_vector(11 downto 0);

begin
        
           -- Addr     data for AS1107.
           -- D11-D8 & D7:D0
           --Initialization
sg_mem_data <= x"0" & "00000000" when mem_addr = x"00" else -- No-Op
               x"1" & "01010101" when mem_addr = x"01" else -- Digit 0
               x"2" & "10101010" when mem_addr = x"02" else -- Digit 1
               x"3" & "01010101" when mem_addr = x"03" else -- Digit 2
               x"4" & "10101010" when mem_addr = x"04" else -- Digit 3
               x"5" & "01010101" when mem_addr = x"05" else -- Digit 4
               x"6" & "10101010" when mem_addr = x"06" else -- Digit 5
               x"7" & "01010101" when mem_addr = x"07" else -- Digit 6
               x"8" & "10101010" when mem_addr = x"08" else -- Digit 7
               x"9" & "00000000" when mem_addr = x"09" else -- Decode mode: no decode.
               x"A" & "00001111" when mem_addr = x"0A" else -- Intensity control: max on.
               x"B" & "00000111" when mem_addr = x"0B" else -- Scan limit: Digits 0-7.
               x"C" & "00000001" when mem_addr = x"0C" else -- Shutdown: Normal Op, Feature reg default.
               x"F" & "00000000" when mem_addr = x"0D" else -- Display test: Normal Op.
       
               x"0" & "00000000"; 
 
 mem_data <= "0000" & sg_mem_data;





 
	  
end Behavioral;

