
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity fsm_as1107 is
  port(
    clk, rst      :  in std_logic;
    start         :  in std_logic;
    en_spi        :  in std_logic;
    
    start_spi     : out std_logic;
    mem_addr      : out std_logic_vector(7 downto 0) );
end fsm_as1107;

architecture Behavioral of fsm_as1107 is

type states is (s00,s01,s02,s03);
signal present_state, next_state: states;

signal dd, qq      : std_logic_vector(4 downto 0);
signal busy, dn_tx : std_logic;

signal sg_en_mem_addr : std_logic;
signal sg_mem_addr    : std_logic_vector(7 downto 0) :=(others=>'0');


begin


---------------------
-- Compteur de cycles (cc)
---------------------
-- Overflow pour clock cycles
mux_ovf: 
  dd <= (others=>'0') when (qq = x"14") else qq + 1;

-- Registre du compteur
process (clk, rst)
begin  
  if (rst = '1') then
    qq <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (en_spi='1') and (busy = '1') then
      qq <= dd;
    else
      qq <= qq;
    end if;
  end if;
end process;

-- Done aprés 20 cycles
dn_tx <= '1' when ((qq > x"11") and (qq < x"13")) else '0';



--===================
-- Present State
--===================
process(clk, rst)
  begin
  if rst='1' then
    present_state <= s00;
  elsif (clk'event and clk='1') then
    if en_spi='1' then
	   present_state <= next_state;
    else		
	   present_state <= present_state;
    end if;
  end if;
end process;

		
--===================
-- Next state logic
--===================
process(present_state, start, dn_tx, sg_mem_addr)
  begin 
  case present_state is 
    -- IDLE
    when s00 =>
      if (start = '1') then
        next_state <= s01;
      else 
        next_state <= s00;
      end if;

    -- Send start to fsm_spi
    when s01 =>   
        next_state <= s02;

    -- wait until end of tx.
    when s02 =>
      if dn_tx='0' then
        next_state <= s02;
      else
        next_state <= s03;
      end if;  

    -- Read next memory addr
    when s03 =>
      if (sg_mem_addr < x"0E") then
        next_state <= s01;
      else
        next_state <= s00;
      end if;  
    		
  end case;
end process;
	
	
--===================	
-- Output logic
--===================
--process(present_state)
process(present_state)
  begin
  case present_state is
    -- IDLE
    when s00 =>
      start_spi      <= '0';
      sg_en_mem_addr <= '0'; 
      busy           <= '0';

    -- Send start to fsm_spi
    when s01 =>
      start_spi      <= '1';
      sg_en_mem_addr <= '0';
      busy           <= '0';
      
    -- wait until end of tx.      
    when s02 =>
      start_spi      <= '0';
      sg_en_mem_addr <= '0';
      busy           <= '1';

    -- Read next memory addr
    when s03 =>
      start_spi      <= '0';
      sg_en_mem_addr <= '1';
      busy           <= '0';
      				
  end case;
end process;


---------------------
-- Generate memory address
---------------------
process (clk, rst)
begin  
  if (rst = '1') then
    sg_mem_addr <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (en_spi='1') and (sg_en_mem_addr = '1') then
      sg_mem_addr <= sg_mem_addr + 1;
    else
      sg_mem_addr <= sg_mem_addr;
    end if;
  end if;
end process;

mem_addr <= sg_mem_addr;
		  
end Behavioral;